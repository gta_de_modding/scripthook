#include <Windows.h>
#include <iostream>
#include <string>
#include <cstdint>
#include <vector>
#include <thread>
#include <chrono>
using namespace std::chrono_literals;

#include "memory.h"

struct CRunningScript
{
public:
	CRunningScript* next;
	CRunningScript* prev;
	char name[8];
	char* baseIP;
	char* currentIP;
	char stackCurrentIP[64];
	int nSP;
	int localVars[42];
	char pad[1];
	bool ret;
};
constexpr auto offs = offsetof(CRunningScript, CRunningScript::ret);

CRunningScript dummyScr{ 0 };

using ProcessCommandType = int64_t(*)(CRunningScript*, int);
ProcessCommandType* ProcessCommandTable;

struct ScrVar {
public:
	enum Type { Int32, Uint32, Float };

	void Set(float*		val) {
		type = Float;
		data = (void*)val;
	}

	void Set(int32_t*	val) {
		type = Int32;
		data = (void*)val;
	}

	void Set(uint32_t*	val) {
		type = Uint32;
		data = (void*)val;
	}

	void Store(void* _data)
	{
		if(type == Int32 || type == Uint32 || type == Float)
			*(uint32_t*)data = *(uint32_t*)_data;
	}

	Type type;
	void* data;
};

struct ScrArg {
public:
	//~ScrArg() { delete[] data; }
	ScrArg(uint8_t      val) { PushData((uint8_t*)&val, 1, 4); }
	ScrArg(int8_t       val) { PushData((uint8_t*)&val, 1, 4); }
	ScrArg(uint16_t     val) { PushData((uint8_t*)&val, 2, 5); }
	ScrArg(int16_t      val) { PushData((uint8_t*)&val, 2, 5); }
	ScrArg(uint32_t     val) { PushData((uint8_t*)&val, 4, 1); }
	ScrArg(int32_t      val) { PushData((uint8_t*)&val, 4, 1); }
	ScrArg(float        val) { PushData((uint8_t*)&val, 4, 6); }
	ScrArg(double       val) { PushData((uint8_t*)&val, 4, 6); }

	ScrArg(char*		val) { PushData((uint8_t*)val, strlen(val), 14, true); }
	ScrArg(const char*	val) { PushData((uint8_t*)val, strlen(val), 14, true); }

	ScrArg(uint32_t*	val) { var.Set(val); isVar = true; }
	ScrArg(int32_t*     val) { var.Set(val); isVar = true; }
	ScrArg(float*       val) { var.Set(val); isVar = true; }

	void PushData(uint8_t* val, uint8_t _size, uint8_t type, bool hasLen = false) {
		size = _size + 1 + (hasLen ? 1 : 0);
		data = new uint8_t[size];
		data[0] = type;
		if (hasLen) data[1] = _size;
		memcpy(&data[1 + (hasLen ? 1 : 0)], val, size - 1 - (hasLen ? 1 : 0));
	}

	uint8_t*	GetData() { return data; }
	size_t		GetSize() { return size; }
	bool		IsVar()	  { return isVar; }
	ScrVar		GetVar()  { return var; }

private:
	uint8_t* data;
	size_t   size;
	bool isVar = false;
	ScrVar var;
};

struct ScrStack {
public:
	ScrStack() : data(nullptr), size(0) {}
	//~ScrStack() { delete[] data; }

	void PushArg(uint8_t* _data, uint8_t _size) {
		uint8_t* newData = new uint8_t[size + _size];
		if (data) {
			memcpy(newData, data, size);
			delete[] data;
		}
		memcpy(&newData[size], _data, _size);
		data = newData;
		size = size + _size;
	}

	void PushVar(ScrVar var, uint16_t idx) {
		uint8_t type = 3;
		PushArg(&type, 1);
		PushArg((uint8_t*)&idx, 2);

		vars.push_back(var);
	}

	void StoreVars(CRunningScript* scr) {
		uint8_t idx = 0;
		for (auto var : vars) {
			var.Store((void*)&scr->localVars[idx]);
			idx++;
		}
	}

	uint8_t* GetData() { return data; }
	size_t   GetSize() { return size; }

private:
	uint8_t* data;
	size_t   size;

	std::vector<ScrVar> vars;
};

template <class ...Args>
ScrStack GetStack(const Args&... args)
{
	ScrStack stack{};
	std::vector<ScrArg> vecArgs = { args... };
	uint16_t vars = 0;
	for (auto& arg : vecArgs) {
		if (arg.IsVar()) {
			stack.PushVar(arg.GetVar(), vars++);
		}
		else {
			stack.PushArg(arg.GetData(), arg.GetSize());
		}
	}
	return stack;
}

template <class ...Args>
bool ExecuteCommand(uint16_t opcode, Args... args)
{
	auto stack = GetStack(ScrArg(args)...);
	dummyScr.baseIP = dummyScr.currentIP = (char*)stack.GetData();
	ProcessCommandTable[opcode / 100](&dummyScr, opcode);
	stack.StoreVars(&dummyScr);
	return dummyScr.ret;
}

static void(*CTheScripts__ProcessOrig)();
void CTheScripts__Process() {
    CTheScripts__ProcessOrig();

	if (GetAsyncKeyState('T') & 0x8000) {
		ExecuteCommand(0x0247, 411); //0247 -> request_model
		ExecuteCommand(0x038B); //038B -> load_all_requested_models

		int car;
		ExecuteCommand(0x00A5, 411, 2487.6621f, -1667.9333f, 13.3329f, &car); //00A5 -> create_vehicle
		std::cout << "car: " << car << std::endl;

		ExecuteCommand(0x0175, car, 69.f); //0175 -> set_car_heading

		float x, y, z;
		ExecuteCommand(0x00AA, car, &x, &y, &z); //00AA -> get_car_pos
		std::cout << "car pos: " << x << " " << y << " " << z << std::endl;
	}
}

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
                     )
{
	ProcessCommandTable = memory(0x418A140).as<ProcessCommandType*>();

    if(ul_reason_for_call == DLL_PROCESS_ATTACH)
	{
		/*AllocConsole();

		FILE* unused = nullptr;
		freopen_s(&unused, "CONIN$", "r", stdin);
		freopen_s(&unused, "CONOUT$", "w", stdout);
		freopen_s(&unused, "CONOUT$", "w", stderr);*/

		memory(0xF2CC40).hook(CTheScripts__Process, &CTheScripts__ProcessOrig);

    }
    return TRUE;
}


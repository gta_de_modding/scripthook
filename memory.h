#pragma once
#include <cstdint>
#include "MinHook/include/MinHook.h"

class memory {
public:
	uintptr_t address;
	memory(uintptr_t offset) : address(((uintptr_t)GetModuleHandle(NULL) + offset)){}

	template <typename T>
	inline std::enable_if_t<std::is_pointer_v<T>, T> as()
	{
		return static_cast<T>((void*)address);
	}

	template <typename T>
	inline std::enable_if_t<std::is_lvalue_reference_v<T>, T> as()
	{
		return *static_cast<std::add_pointer_t<std::remove_reference_t<T>>>((void*)address);
	}

	template <typename T>
	inline std::enable_if_t<std::is_same_v<T, std::uintptr_t>, T> as()
	{
		return reinterpret_cast<std::uintptr_t>((void*)address);
	}

	template <typename T>
	inline memory add(T offset)
	{
		return memory(as<std::uintptr_t>() + offset);
	}

	memory rip()
	{
		return add(as<std::int32_t&>()).add(4);
	}

	void nop(size_t len)
	{
		scoped_unfuck lock(address, len);
		memset((void*)address, 0x90, len);
	}

	void ret()
	{
		put<uint8_t>(0xC3);
	}

	template<typename T>
	void put(const T& value)
	{
		scoped_unfuck lock(address, sizeof(T));
		memcpy((void*)address, &value, sizeof(T));
	}

	template<class T>
	void hook(T* target, T** orig = nullptr)
	{
		MH_Initialize();
		MH_CreateHook((void*)address, (void*)target, (void**)orig);
		MH_EnableHook((void*)address);
	}

private:
	struct scoped_unfuck
	{
		DWORD rights;
		size_t len;
		void* addr;

		scoped_unfuck(uint64_t _addr, size_t _len) :addr((void*)_addr), len(_len)
		{
			VirtualProtect(addr, len, PAGE_EXECUTE_READWRITE, &rights);
		}

		~scoped_unfuck()
		{
			VirtualProtect(addr, len, rights, NULL);
		}
	};
};